<?php 
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Library\DeomarClass as DC;
use Validator;

class MainController extends Controller{

    public function index(Request $request){
        $jointTable = $request->joinTable;
        $model = $request->model;
               
        if($request->column){
            if($jointTable){
                $row = DC::model($model)->with($jointTable)->where($request->column,$request->search)->first();  
            }
            else{
                $row = DC::model($model)->where($request->column,$request->search)->first();   
            }
                    
        }
        else{
            if($jointTable){
                $row = DC::model($model)->with($jointTable)->get();
            }
            else{
                $row = DC::model($model)->get();
            }
        }
        return $row;
    }

    public function store(Request $request, $model){      
       
        if($request->id){
            $update = DC::model($model)->find($request->id);
            $array_to_save = array_except($request->all(),'id');
            $update->update($array_to_save);
            return $array_to_save;
        }
        else{
            $array_to_save = $request->all();
            return DC::model($model)->create($array_to_save);
        }
        
    }

    public function upload(Request $request, $model){      
        if ($request->image) {
            $filename =  $request->image->getClientOriginalName();
            Storage::disk('public')->putFileAs('images/'.DC::foldername($model), $request->image, $request->image->getClientOriginalName());
            $data = [
                'image_path'=>$filename,
            ];
            $update = DC::model($model)->find($request->id);
            $update->update($data);            
        }
    }
    
    public function destroy(Request $request){
        $id=$request->id;
        $query = DC::model($request->model)->find($id);
        $query->delete();
        return $query;
    }
}

?>