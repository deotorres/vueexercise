<?php

namespace App\Library;

class DeomarClass {
    
    public static function model($model){

        $model_name = "\\App\\Model\\$model";
		return new $model_name();

    }
    public static function foldername($model){
        $folder_name = strtolower($model);
        return $folder_name;
    }
} 


?>