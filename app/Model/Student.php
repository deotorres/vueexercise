<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Student extends Model
{
    protected $fillable = ['firstname','middlename','lastname','student_id','contact_number','year_level','course_id'];

    public function course(){
        return $this->belongsTo('App\Model\Course');
    }
}
