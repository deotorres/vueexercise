import App from './components/App'

import Vue from 'vue'

import router from './routes';



const app = new Vue({
    el:'#app',
    components:{
        App
    },
    router,
    created(){
        console.log("run vue");
    }
})
