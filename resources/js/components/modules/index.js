export { default as Archive } from './Archive';
export { default as Home } from './Home';
export { default as Main } from './Main';
export { default as Course } from './Course';
export { default as Student } from './Student';
export { default as School } from './School';