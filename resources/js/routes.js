import Vue from 'vue'
import VueRouter from 'vue-router';

import ExampleComponent from './components/ExampleComponent';

import {Archive,Home,Main, Course, Student, School} from './components/modules'
Vue.use(VueRouter);
const routes = [
    {
        path:'/',
        name:'HomeComponent',
        component:Home
    },
    {
        path:'/view',
        name:'exampleComponent',
        component:ExampleComponent
    },
    {
        path:'/main',
        name:'TableComponent',
        component:Main
    },
    {
        path:'/archives',
        name:'ArchivesComponent',
        component:Archive
    },
    {
        path:'/courses',
        name:'CourseComponent',
        component:Course
    },
    {
        path:'/students',
        name:'StudentsComponent',
        component:Student
    },
    {
        path:'/schools',
        name:'SchoolsComponent',
        component:School
    }
]
const router = new VueRouter(
    {
        mode:"history",
        routes:routes
    }
)

export default router;