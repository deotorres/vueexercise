<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(["namespace"=>"api"],function(){
    Route::get("alldata/{model}/{column?}/{search?}/{joinTable?}","MainController@index");
    Route::post("store/{model}","MainController@store");
    Route::post("upload/{model}","MainController@upload");
    Route::post("delete/{model}/","MainController@destroy");
});